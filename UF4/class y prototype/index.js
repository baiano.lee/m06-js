class Car {
  constructor(name, speed){
  this._name = name;
  this._speed = speed;
  }

get speedUS(){
  return this._speed/1.6;
}

set speedUS(newSpeed){
  this._speed = newSpeed*1.6;
}

veloc(){
  this._speed += 10;
}

break(){
  this._speed -= 5;
}

}


class EV extends Car{
  constructor(name, speed, charge){
    super(name, speed);
    this._charge = charge;
  }

  set chargeBattery(chargeTo){
    this._charge = chargeTo;
  }

  accelerate(){
    this._speed += 20;
    this._charge -= 1;
  }


}

const tesla = new EV("Tesla", 110, 23);
tesla.accelerate();

tesla.veloc();

console.log(`Tesla going at ${ tesla._speed} km/h, with a charge of ${ tesla._charge}%`);

const Car1 = function(name, speed){
  this.name = name;
  this.speed = speed;
}

Car1.prototype.veloc = function(){
  console.log(10 + this.speed);
}

Car1.prototype.break = function(){
  console.log(5 - this.speed);
}



const bmw = new Car1("BMW", 0);
console.log(bmw);

const mercedes = new Car1("Mercedes", 0);
console.log(mercedes);

mercedes.veloc();
bmw.veloc();

mercedes.break();

//Person.prototype.pais = "Esp";


//console.log(jhon.pais);
