const app = Vue.createApp({
  data(){
    return {
      list: [],
      messageData: ''
    };
  },
  methods: {
    addGoal() {
      this.list.push(this.messageData); 
      this.messageData = '';
    }
  }
})
app.mount('#Myapp');